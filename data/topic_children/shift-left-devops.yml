description: Continuous integration (CI) is a process that improves code quality
  through deployment pipelines. Security can be integrated into these pipelines
  earlier in the process, helping organizations shift left.
canonical_path: /topics/ci-cd/shift-left-devops/
parent_topic: ci-cd
file_name: shift-left-devops
twitter_image: /images/opengraph/gitlab-blog-cover.png
title: How to shift left with continuous integration
header_body: Continuous integration (CI) is a process that improves code quality
  through deployment pipelines. Security can be integrated into these pipelines
  earlier in the process, helping organizations shift left.
body: >-
  ## How to shift left with continuous integration


  Shift left is an approach that moves testing to earlier in the software development lifecycle (hence, “shifting left”). If security testing happens when code is ready for production, it can be difficult to go back and correct problems, and it’s often too late to fix problems quickly. This can lead to delayed handoffs, security issues, and silos between security and the rest of the DevOps teams.


  As organizations try to move to a more [DevSecOps](https://about.gitlab.com/solutions/dev-sec-ops/) structure, bringing security testing earlier into the development lifecycle will be critical. The way to do this is by integrating security testing into deployment pipelines so that code is continually tested, not only against other commits into the shared repository, but for security as well.


  [Continuous integration (CI)](/topics/ci-cd/) is a process that improves code quality through deployment pipelines. Security can be integrated into these pipelines earlier in the process, helping organizations shift left.


  ### Integrate security into continuous integration pipelines


  [Static Application Security Testing](https://docs.gitlab.com/ee/user/application_security/sast/) (SAST) is one way that security is automated through continuous integration. SAST analyzes the source code and allows developers to fix problems earlier in the software development lifecycle.


  In GitLab CI/CD, the deployment pipeline checks the SAST report and compares the vulnerabilities between the source and target branches. These findings appear in the merge request.


  ![SAST example](https://docs.gitlab.com/ee/user/application_security/sast/img/sast_v13_2.png)


  [Dynamic Application Security Testing](https://docs.gitlab.com/ee/user/application_security/dast/index.html#dynamic-application-security-testing-dast) (DAST) often works in tandem with SAST. While SAST analyzes source code, DAST analyzes runtime errors in executed applications. Once an application is deployed, it is exposed to new forms of security risks, such as cross-site scripting or broken authentication flaws.


  Like SAST, GitLab checks the DAST report and compares the vulnerabilities between the source and target branches and displays the results, but the comparison uses only the latest pipeline executed for the target branch’s base commit.


  ![DAST example](https://docs.gitlab.com/ee/user/application_security/dast/img/dast_v13_4.png)


  Other [types of security testing](https://www.softwaresecured.com/what-do-sast-dast-iast-and-rasp-mean-to-developers/) include Interactive Application Security Testing (IAST) and Run-Time Application Security Protection (RASP). IAST operates by placing an agent within an application and RASP is more of a security tool placed inside an application that can respond to live attacks.


  ### Reduce toolchain complexity


  In addition to time-consuming maintenance, [a complex toolchain](https://about.gitlab.com/blog/2019/11/20/toolchain-security-with-gitlab/) can open up a system to security risks. Many DevSecOps teams use plugins, scripts, or hard-coded custom integrations to bring their tools together. Since some of these have to be done manually, it makes these toolchains subject to human error. Additionally, more tools mean more authentication, more permissions, security requirements, and less visibility into the software development lifecycle. These layers of abstraction make it harder to not only pinpoint problems, but solve them as well.


  A complex system incorporates multiple points of failure. If organizations want to shift left, reducing some of this complexity makes it easier for security and compliance to come into the development lifecycle. A complex toolchain or a plugin environment can also cause [brittle pipelines](https://harness.io/2018/09/4-reasons-your-jenkins-pipelines-are-brittle/) that need extra attention.


  ### Harden your continuous integration systems


  [Hardening](https://en.wikipedia.org/wiki/Hardening_computing) is the process of securing a system by reducing its surface of vulnerability. Similar to reducing toolchain complexity to reduce the sources of risk, hardening checklists allow an organization to examine its internal systems to make sure they’re following security best practices.


  One recommendation is to harden the [systems that host](https://about.gitlab.com/blog/2019/10/30/secure-journey-continuous-delivery/) the source and build artifact repositories, the CI and continuous delivery (CD) servers, and the systems that host the configuration management, build, deployment, and release tools. Ensure that your team knows what is done on-premises vs. what is in the cloud, and how this impacts workflows.


  Hardening your continuous integration system, in addition to incorporating security scans into your deployment pipelines, can make it easier for teams to shift left. [Mature DevOps teams](https://about.gitlab.com/topics/devops/build-a-devops-team/) are naturally implementing security tests into their continuous integration process and embracing the shift left approach. Rather than treating security as an afterthought, these DevSecOps teams keep security top of mind.
suggested_content:
  - url: /blog/2020/01/27/ciso-secure-next-gen-software/
  - url: /blog/2019/05/03/secure-containers-devops/
  - url: /blog/2020/08/21/align-engineering-security-appsec-tests-in-ci/
