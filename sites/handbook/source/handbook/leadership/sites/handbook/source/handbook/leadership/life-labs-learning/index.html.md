---
layout: handbook-page-toc
title: LifeLabs Learning
description: "Manager Enablement Training with LifeLabs Learning"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

### Introduction

As part of GiLab's focus on equipping people leaders with the skills and tools to lead all-remote teams, L&D has partnered with [LifeLabs Learning](https://lifelabslearning.com/) to deliver management training. The program is intended to provide additional training for managers as part of their management journey and customized GitLab management programs (i.e. [Manager Challenge](https://about.gitlab.com/handbook/people-group/learning-and-development/manager-challenge/)) 

### Goal

The goal of LifeLabs Learning is to give people leaders an opportunity to build and reinforce core management skills. GitLab requires exceptional leaders and powerful teams. And managers need the most essential skills in the shortest time. 

As a participant in LifeLabs learning, you will gain the essential building blocks of a high-performing manager, leading all-remote teams. 

### What it is not

Participating in LifeLabs Learning is not a ticket to a promotion or raise. Instead, it is a growth & development opportunity that will lead to skill development to help improve your leadership skills. 

### Benefits for the company

Apart from creating leadership development opportunities, participating in LifeLabs Learning will: 

1. Improve employee engagement
2. Increase growth & development opportunities for managers and up
3. Improve communication by managers to direct reports on providing feedback, solving problems, motivating team members, performance management, and resolving conflicts

### The program

LifeLabs Learning offers two core manager development programs. Each workshop is 2 hours, with a 10 participant max. 

#### [Manager Core 1 Curriculum](https://drive.google.com/file/d/1MJmxjrMSSCq3lWOOks-vMnzdzPucI0jp/view)

One Session per week: 
1. Coaching Skills: Master the cornerstone skill of great leaders, developing people to become high performers
2. Feedback Skills: Give performance changing feedback even when the topic is tough and time is limited
3. Productivity & Prioritization: Collect tools to help you and your team make progress on the most important initatives
4. Effective 1-1's: Use 1:1's to maximize people's feelings of certainty, progress, inclusion, growth, and engagement

3 months later: 
5. Manager Intensive 1: Assess your skills developed in the workshop and apply them to the next level 

#### Manager Core 2 Curriculum

The [Manager Core 2 program](https://drive.google.com/file/d/1f0HhqBfGn1lnaYSMHcYYqauwyOkNHS0R/view) is recommended to take at least 3 months or later after the Manager Intensive 1 has been completed. 

One session per week: 
1. Strategic Thinking: Apply strategic thinking tools to real-world projects
2. Meetings Mastery: Use advanced facilitation tools and techniques
3. Leading Change: Plan and message change initiatives
4. People Development: Succession planning and holding career growth conversations 

3 months later: 
5. Manager Intensive 2: Refresher on topics covered in the core 2 program

### Participating in the program

#### Eligibility 

You are eligible to apply for the program if you are a: 
 
1. [Manager or up](https://about.gitlab.com/company/team/structure/#layers), [Product Manager or up](https://about.gitlab.com/job-families/product/product-manager/), Staff Engineer


#### Pilot Schedule

In FY23Q1-Q2, a pilot of LifeLabs Learning is taking place with 10 cross-functional people leaders across the organization: 

Below is the schedule for the pilot program: 

| Workshop | Date | Time | 
| ------ | ------ | ------ |
| Coaching Skills | Thursday 2022-01-13 | 8am-10am PST / 11am-1pm EST / 4pm-6pm GMT |
| Feedback Skills | Thursday 2022-01-27 | 8am-10am PST / 11am-1pm EST / 4pm-6pm GMT |
| Productivity & Prioritization | Thursday 2022-02-17 | 8am-10am PST / 11am-1pm EST / 4pm-6pm GMT |
| Effective 1:1s | Wednesday 2022-02-23 | 8am-10am PST / 11am-1pm EST / 4pm-6pm GMT |
| Manager Intensive 1 | Thursday 2022-05-26 | 8am-10am PST / 11am-1pm EST / 4pm-6pm GMT |

#### FY23 Schedule 

L&D is rolling out four cohorts of LifeLabs Learning in FY23. The program will be broken down by the following: 

**Manager Core 1 Workshops**

Proposed Start Date: 2022-04-01
Session Dates: TBD

**Manager Core 2 Workshops**

Proposed Start Date: 2022-09-01
Session Dates: TBD

#### How to apply

If interested in participating in LifeLabs Learning Manager Core 1 and Core 2 programs, please contact the learning & development team in the `#learninganddevelopment` slack channel. You can also reach out to your respective [People Business Partner](/handbook/people-group/#people-business-partner-alignment-to-division)

### Preparing for the training

#### Pre-Work:
 
After you sign-up for the program, you can expect to see calendar invites posted for each session on your calendar. Each session has pre-work in the form of reflection. Managers are asked to reflect back on their experience and come prepared to share that experience with the broader group. More details can be found in the session invites. 

